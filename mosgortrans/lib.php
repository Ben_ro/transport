<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
require_once 'simple/simple_html_dom.php';
//http://mosgortrans.org/pass3/request.ajax.php?list=ways&type=avto
//http://mosgortrans.org/pass3/request.ajax.php?list=days&type=avto&way=15
//http://mosgortrans.org/pass3/request.ajax.php?list=directions&type=avto&way=15&date=1111100
//http://mosgortrans.org/pass3/request.ajax.php?list=waypoints&type=avto&way=15&date=1111100&direction=AB
//http://mosgortrans.org/pass3/shedule.php?type=avto&way=15&date=1111100&direction=AB&waypoint=1
class html {
	public $requestUrl;
	public $sheduleUrl;
	function __construct(){
		$this->requestUrl = 'http://mosgortrans.org/pass3/request.ajax.php';
		$this->sheduleUrl = 'http://mosgortrans.org/pass3/shedule.php';
	}
	function get_way($tvalue){
		if($tvalue){
			$url = $this->requestUrl."?list=ways&type={$tvalue}";
			$html = file_get_html($url);
			$array = explode(' ', mb_convert_encoding($html->innertext, "utf-8", "windows-1251"));
			echo $this->safe_json_encode($array);
		}
	}
	function get_days($tvalue, $wvalue){
		if($tvalue and $wvalue){
			$url = $this->requestUrl."?list=days&type={$tvalue}&way={$wvalue}";
			$html = file_get_html($url);
			$array = explode(' ', mb_convert_encoding($html->innertext, "utf-8", "windows-1251"));
			$date = array();
			foreach($array as $day){
				switch($day){
					case '1111111':
						$date['1111111'] = 'Все дни недели';
						break;
					case '0000011':
						$date['0000011'] = 'Выходные';
						break;
					case '1111100':
						$date['1111100'] = 'Будние';
						break;
				}
			}
			echo $this->safe_json_encode($date);
			
		}
	}
	function get_dir($tvalue, $wvalue, $dvalue){
		if($tvalue and $wvalue and $dvalue){
			$url = $this->requestUrl."?list=directions&type={$tvalue}&way={$wvalue}&date={$dvalue}";
			$html = mb_convert_encoding(file_get_contents($url), "utf-8", "windows-1251");
			$array = explode(PHP_EOL, $html);
			$dir['AB'] = $array[0];
			$dir['BA'] = $array[1];
			echo $this->safe_json_encode($dir);
			
		}
	}
	function get_stand($tvalue, $wvalue, $dvalue, $divalue){
		if($tvalue and $wvalue and $dvalue and $divalue){
			$url = $this->requestUrl."?list=waypoints&type={$tvalue}&way={$wvalue}&date={$dvalue}&direction={$divalue}";
			$html = mb_convert_encoding(file_get_contents($url), "utf-8", "windows-1251");
			$array = explode(PHP_EOL, $html);
			
			echo $this->safe_json_encode($array);
			
		}
	}
	function get_table($url, $params){
		if($url){
			$htm = mb_convert_encoding(file_get_contents($url), "utf-8", "windows-1251");
			$html = str_get_html($htm);
			$param = json_decode($params);
			foreach($param as $line){
				$type[$line[0]] = $line[1];
			}
			$one = $html->find('td[class=bottomwideborder]')[1];
			$table = $one->find('table>tr');
			foreach($table as $tr){
				$right = $tr->find('td[align=right]');
				$left = $tr->find('td[align=left]');
				foreach($right as $key => $td){
					if($td->find('span')[0]->class == 'hour'){
						if(iconv_strlen($td->find('span')[0]->plaintext) == 1){
							$hour = "0{$td->find('span')[0]->plaintext}";
						}else{
							$hour = $td->find('span')[0]->plaintext;
						}
						foreach($left[$key]->find('span') as $keys => $span){
							$time[$hour][$keys] = $span->plaintext;
						}
						
					}
					
				}
			}
			$hcount = count($time);
			$i = 0;
			foreach($time as $key => $hour){
				if($i > 1 and $i < $hcount-1 and !$type['spisok']){
					$mcount = count($hour);
					$raz = [];
					if($mcount >= 3){
						foreach($hour as $keys => $min){
							if($mcount > 1){
								if($keys+1 < $mcount){
									$raz[] = $hour[$keys+1] - $min;
								}
							}
						}
						if(!empty($raz)){
							$max = max($raz);
							$min = min($raz);
							if($max <= 20){
								if($min == $max){
									$time[$key] = ["{$min} _мин/min"];
								}else{
									$time[$key] = ["{$min}-{$max} _мин/min"];
								}
							}
						}
					}
				}else{
					$i++;
				}
			}
			
			foreach($time as $key => $hour){
				echo $key.' | ';
				$cmin = count($hour);
				if($cmin % 2 != 0){
					$firs = intdiv($cmin, 2);
				}else{
					$firs = intdiv($cmin, 2)-1;
				}
				$i = 1;
				foreach($hour as $keys => $min){
					
					if($keys+1 == $cmin){
						if($cmin == 1){
							if(strripos($min, 'min') !== false){
								echo $min;
							}else{
								echo $min.' _';
							}
						}else{
							echo $min;
						}
								
					}else{
						if($i <= $firs){
							echo $min.' ';
							$i++;
						}else{
							echo $min.' _';
							$i = 1;
						}
					}
				}
				echo '<br>';
			}
			foreach($html->find('p[class=helpfile]') as $p){
				echo $p->outertext;
			}
			
		}
	}
	function safe_json_encode($value){
		if (version_compare(PHP_VERSION, '5.4.0') >= 0) {
			$encoded = json_encode($value, JSON_PRETTY_PRINT, JSON_UNESCAPED_UNICODE);
		} else {
			$encoded = json_encode($value, JSON_UNESCAPED_UNICODE);
		}
		switch (json_last_error()) {
			case JSON_ERROR_NONE:
				return $encoded;
			case JSON_ERROR_DEPTH:
				return 'Maximum stack depth exceeded';
			case JSON_ERROR_STATE_MISMATCH:
				return 'Underflow or the modes mismatch';
			case JSON_ERROR_CTRL_CHAR:
				return 'Unexpected control character found';
			case JSON_ERROR_SYNTAX:
				return 'Syntax error, malformed JSON';
			case JSON_ERROR_UTF8:
				$clean = $this->utf8ize($value);
				return $this->safe_json_encode($clean);
			default:
				return 'Unknown error'; 
		Exception();
		}
	}
	function utf8ize($mixed) {
		if (is_array($mixed)) {
			foreach ($mixed as $key => $value) {
				$mixed[$key] = $this->utf8ize($value);
			}
		} else if (is_string ($mixed)) {
			return utf8_encode($mixed);
		}
		return $mixed;
	}
}

﻿<!DOCTYPE html>
<html>
 <head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title>Тег META, атрибут charset</title>
  <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
	<script src="script.js"></script>
 </head>
 <body> 

<?php
require('lib.php');
?>
<select name='type' id='type' onchange="types();">
	<option></option>
	<option value='avto'>Автобус</option>
	<option value='tram'>Трамвай</option>
	<option value='trol'>Троллейбус</option>
</select>
<select name='number' id='number' onchange='number()'>
	<option></option>
</select>
<select name='day' id='day' onchange='day()'>
	<option></option>
</select>
<select name='direction' id='direction' onchange='direction()'>
	<option></option>
</select>
<select name='stand' id='stand' onchange='stand()'>
	<option></option>
</select>
<input type="checkbox" id="spisok" name="spisok" >
  <label for="spisok">Не объединять в интервалы</label>
<br><br>
<div id='items'></div>

 </body>
</html>
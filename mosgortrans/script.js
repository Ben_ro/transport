var trans = [];
for (var i = 0x410; i <= 0x44F; i++)
  trans[i] = i - 0x350; // А-Яа-я
trans[0x401] = 0xA8;    // Ё
trans[0x451] = 0xB8;    // ё

var escapeOrig = window.escape;

window.escape = function(str)
{
  var ret = [];
  for (var i = 0; i < str.length; i++)
  {
    var n = str.charCodeAt(i);
    if (typeof trans[n] != 'undefined')
      n = trans[n];
    if (n <= 0xFF)
      ret.push(n);
  }
  return escapeOrig(String.fromCharCode.apply(null, ret));
}
function types(){
	$.post('get.php', {
		type: 'type',
		tvalue: $('#type').val()
	})
	.done(function(data) {
		var array = JSON.parse(data);
		$('#number').html('<option></option>');
		$('#day').html('<option></option>');
		$('#direction').html('<option></option>');
		$('#stand').html('<option></option>');
		$.each(array, function (index, val){
			$('#number').append('<option value="'+val+'">'+val+'</option>')
		})
	})
}
function number(){
	$.post('get.php', {
		type: 'number',
		tvalue: $('#type').val(),
		wvalue: $('#number').val()
	})
	.done(function(data) {
		var array = JSON.parse(data);
		$('#day').html('<option></option>');
		$('#direction').html('<option></option>');
		$('#stand').html('<option></option>');
		$.each(array, function (index, val){
			$('#day').append('<option value="'+index+'">'+val+'</option>')
		})
	})
}
function day(){
	$.post('get.php', {
		type: 'day',
		tvalue: $('#type').val(),
		wvalue: $('#number').val(),
		dvalue: $('#day').val()
	})
	.done(function(data) {
		var array = JSON.parse(data);
		$('#direction').html('<option></option>');
		$('#stand').html('<option></option>');
		$.each(array, function (index, val){
			$('#direction').append('<option value="'+index+'">'+val+'</option>')
		})
	})
}
function direction(){
	$.post('get.php', {
		type: 'direction',
		tvalue: $('#type').val(),
		wvalue: $('#number').val(),
		dvalue: $('#day').val(),
		divalue: $('#direction').val()
	})
	.done(function(data) {
		var array = JSON.parse(data);
		$('#stand').html('<option></option>');
		$.each(array, function (index, val){
			$('#stand').append('<option value="'+index+'">'+val+'</option>')
		})
	})
}
function stand(){
	var url = 'http://mosgortrans.org/pass3/shedule.php?type='+$('#type').val()+'&way='+escape($('#number').val())+'&date='+$('#day').val()+'&direction='+$('#direction').val()+'&waypoint='+$('#stand').val()
	array = [
		['spisok', $("#spisok").is(':checked')]
	]
	params = JSON.stringify(array);
	console.log(url);
	$.post('get.php', {
		type: 'stand',
		url: url,
		params: params
	})
	.done(function(data) {
		$('#items').html(data)		
	})
}
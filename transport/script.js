var trans = [];
for (var i = 0x410; i <= 0x44F; i++)
  trans[i] = i - 0x350; // А-Яа-я
trans[0x401] = 0xA8;    // Ё
trans[0x451] = 0xB8;    // ё

var escapeOrig = window.escape;

window.escape = function(str)
{
  var ret = [];
  for (var i = 0; i < str.length; i++)
  {
    var n = str.charCodeAt(i);
    if (typeof trans[n] != 'undefined')
      n = trans[n];
    if (n <= 0xFF)
      ret.push(n);
  }
  return escapeOrig(String.fromCharCode.apply(null, ret));
}
function types(){
	$.post('get.php', {
		type: 'type',
		tvalue: $('#type').val()
	})
	.done(function(data) {
		var array = JSON.parse(data);
		$('#number').html('<option></option>');
		$('#direction').html('<option></option>');
		$('#stand').html('<option></option>');
		$.each(array, function (index, val){
			$('#number').append('<option value="'+val.id+'">'+val.routeName+'</option>')
		})
	})
}
function day(){
	$.post('get.php', {
		type: 'day',
		wvalue: $('#number').val(),
		dvalue: $('#day').val()
	})
	.done(function(data) {
		var array = JSON.parse(data);
		$('#direction').html('<option></option>');
		$('#stand').html('<option></option>');
		$.each(array, function (index, val){
			$('#direction').append('<option value="'+val.id+'">'+val.direction+'</option>')
		})
	})
}
function direction(){
	$.post('get.php', {
		type: 'direction',
		tvalue: $('#type').val(),
		wvalue: $('#number').val(),
		dvalue: $('#day').val(),
		divalue: $('#direction').val(),
		svalue: $('#season').val()
	})
	.done(function(data) {
		var array = JSON.parse(data);
		$('#stand').html('<option></option>');
		$.each(array, function (index, val){
			$('#stand').append('<option value="'+val.id+'">'+val.stopName+'</option>')
		})
	})
} 
function stand(){
	array = [
		['spisok', $("#spisok").is(':checked')]
	]
	params = JSON.stringify(array);
	$.post('get.php', {
		type: 'stand',
		tvalue: $('#type').val(),
		wvalue: $('#number').val(),
		dvalue: $('#day').val(),
		divalue: $('#direction').val(),
		svalue: $('#season').val(),
		stvalue: $('#stand').val(),
		params: params
	})
	.done(function(data) {
		$('#items').html(data)		
	})
}
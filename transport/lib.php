<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
require_once 'simple/simple_html_dom.php';
//http://lkcar.transport.mos.ru/ExternalService/api/schedule/route?vehicleTypeId=700
//http://lkcar.transport.mos.ru/ExternalService/api/schedule/direction?day=WEEKEND&routeId=1141
//http://lkcar.transport.mos.ru/ExternalService/api/schedule/stop?day=WEEKEND&directionId=0&routeId=1141&season=WINTER&vehicleTypeId=700
//http://lkcar.transport.mos.ru/ExternalService/api/schedule/?day=WEEKDAYS&directionId=1&routeId=1121&season=WINTER&stopId=13714&vehicleTypeId=700
class html {
	public $requestUrl;
	public $sheduleUrl;
	function __construct(){
		$this->requestUrl = 'http://lkcar.transport.mos.ru/ExternalService/api/schedule/';
	}
	function get_way($tvalue){
		if($tvalue){
			$url = $this->requestUrl."route?vehicleTypeId={$tvalue}";
			$html = file_get_contents($url);
			echo $html;
		}
	}
	function get_dir($wvalue, $dvalue){
		if($wvalue and $dvalue){
			$url = $this->requestUrl."direction?day={$dvalue}&routeId={$wvalue}";
			$html = file_get_contents($url);
			echo $html;
			
		}
	}
	function get_stand($tvalue, $wvalue, $dvalue, $divalue, $svalue){
		if($tvalue and $wvalue and $dvalue){
			$url = $this->requestUrl."stop?day={$dvalue}&directionId={$divalue}&routeId={$wvalue}&season={$svalue}&vehicleTypeId={$tvalue}";
			$html = file_get_contents($url);
			echo $html;
			
		}
	}
	function get_table($tvalue, $wvalue, $dvalue, $divalue, $svalue, $stvalue, $params){
			$url = $this->requestUrl."?day={$dvalue}&directionId={$divalue}&routeId={$wvalue}&season={$svalue}&stopId={$stvalue}&vehicleTypeId={$tvalue}";
			$html = file_get_contents($url);
			$array = (array)json_decode($html);
			$param = json_decode($params);
			foreach($param as $line){
				$type[$line[0]] = $line[1];
			}
			$hcount = count($array);
			$i = 0;
			foreach($array as $hour => $mins){
				if($i > 1 and $i < $hcount-1 and !$type['spisok']){
					$mcount = count($mins);
					$raz = [];
					if($mcount >= 3){
						foreach($mins as $key => $min){
							if($mcount > 1){
								if($key+1 < $mcount){
									$raz[] = $mins[$key+1] - $min;
								}
							}
						}
						if(!empty($raz)){
							$max = max($raz);
							$min = min($raz);
							if($max <= 20){
								if($min == $max){
									$array[$hour] = ["{$min} _мин/min"];
								}else{
									$array[$hour] = ["{$min}-{$max} _мин/min"];
								}
							}
						}
					}
				}else{
					$i++;
				}
				if($hour == '0'){
					unset($array[$hour]);
					$array[$hour] = $mins;
				}
				
			}
			foreach($array as $hour => $mins){
				if(iconv_strlen($hour) == 1){
					$hour = "0{$hour}";
				}else{
					$hour = $hour;
				}
				echo $hour.' | ';
				$cmin = count($mins);
				if($cmin % 2 != 0){
					$firs = intdiv($cmin, 2);
				}else{
					$firs = intdiv($cmin, 2)-1;
				}
				$i = 1;
				foreach($mins as $keys => $min){
					
					if($keys+1 == $cmin){
						if($cmin == 1){
							if(strripos($min, 'min') !== false){
								echo $min;
							}else{
								echo $min.' _';
							}
						}else{
							echo $min;
						}
								
					}else{
						if($i <= $firs){
							echo $min.' ';
							$i++;
						}else{
							echo $min.' _';
							$i = 1;
						}
					}
				}
				echo '<br>';
			}

	}
	function safe_json_encode($value){
		if (version_compare(PHP_VERSION, '5.4.0') >= 0) {
			$encoded = json_encode($value, JSON_PRETTY_PRINT, JSON_UNESCAPED_UNICODE);
		} else {
			$encoded = json_encode($value, JSON_UNESCAPED_UNICODE);
		}
		switch (json_last_error()) {
			case JSON_ERROR_NONE:
				return $encoded;
			case JSON_ERROR_DEPTH:
				return 'Maximum stack depth exceeded'; 
			case JSON_ERROR_STATE_MISMATCH:
				return 'Underflow or the modes mismatch';
			case JSON_ERROR_CTRL_CHAR:
				return 'Unexpected control character found';
			case JSON_ERROR_SYNTAX:
				return 'Syntax error, malformed JSON'; 
			case JSON_ERROR_UTF8:
				$clean = $this->utf8ize($value);
				return $this->safe_json_encode($clean);
			default:
				return 'Unknown error'; 
		Exception();
		}
	}
	function utf8ize($mixed) {
		if (is_array($mixed)) {
			foreach ($mixed as $key => $value) {
				$mixed[$key] = $this->utf8ize($value);
			}
		} else if (is_string ($mixed)) {
			return utf8_encode($mixed);
		}
		return $mixed;
	}
}
